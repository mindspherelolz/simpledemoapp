var express = require('express');
var cfenv = require('cfenv');
appEnv = cfenv.getAppEnv();
app = express();

app.use(express.static(__dirname + "/dist"));

app.listen(appEnv.port, appEnv.bind, function() {
    console.log("server starting on " + appEnv.url)
  });
  