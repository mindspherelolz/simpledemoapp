import Vue from 'vue';
import Vuex from 'vuex';

import layout from './layout';
import timeseries from './timeseries'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    layout,
    timeseries
  },
});
