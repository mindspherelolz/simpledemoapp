/* eslint-disable */
import axios from 'axios'

let assestId = 'ed68e42a946441bc8b2a2a54ebb174b2';
let aspectType = 'aspect_Virtual_Motor_Condition';

let timer = null;

export default {
    state: {
      timeseries: [],
      aggregate: [
          {
              name: 'aCrest',
              data:[]
          },
          {
              name: 'Ambient_Temp',
              data:[]
          },
          {
              name: 'Motor_Temp',
              data:[]
          },
          {
              name: 'Raw_Vibration_Signal',
              data:[]
          },
          {
              name: 'vCrest',
              data:[]
          },
      ],
    },
    mutations: {
      async getTimeSeries(state){
        try {
            let res = await axios.get(`/api/iottimeseries/v3/timeseries/${assestId}/${aspectType}`);
            state.timeseries = res.data
        }catch(err) {
            console.log(err);
        }
      },
      async getAggregate(state){
        try{
          // let date = new Date().toISOString().slice(0,10);
          let start = new Date();
          start.setHours(start.getHours() - 8);
          start = start.toISOString().toString().slice(0,13) + ':00:00Z';
          let end = new Date().toISOString().toString().slice(0,13) + ':00:00Z';
          let res = await axios.get(`/api/iottsaggregates/v3/aggregates/${assestId}/${aspectType}?from=${start}&to=${end}&intervalValue=1&intervalUnit=hour`);
                state.aggregate[0].data = [];
                state.aggregate[1].data = [];
                state.aggregate[2].data = [];
                state.aggregate[3].data = [];
                state.aggregate[4].data = [];
                res.data.forEach(data => {
                    let time = data.starttime.slice(11,19);
                    if(data.aCrest != undefined){
                        let aCrest = {
                            x: time,
                            y:Math.floor(data.aCrest.average * 100) / 100
                        }
                        state.aggregate[0].data.push(aCrest);
                    }
                    if(data.Ambient_Temp != undefined){
                        let Ambient_Temp = {
                            x: time,
                            y:Math.floor(data.Ambient_Temp.average * 100) / 100
                        }
                        state.aggregate[1].data.push(Ambient_Temp);
                    }
                    if(data.Motor_Temp != undefined){
                        let Motor_Temp = {
                            x: time,
                            y:Math.floor(data.Motor_Temp.average * 100) / 100
                        }
                        state.aggregate[2].data.push(Motor_Temp);
                    }
                    if(data.Raw_Vibration_Signal != undefined){
                        let Raw_Vibration_Signal = {
                            x: time,
                            y:Math.floor(data.Raw_Vibration_Signal.average * 100) / 100
                        }
                        state.aggregate[3].data.push(Raw_Vibration_Signal);
                    }
                    if(data.vCrest != undefined){
                        let vCrest = {
                            x: time,
                            y:Math.floor(data.vCrest.average * 100) / 100
                        }
                        state.aggregate[4].data.push(vCrest);
                    }
                });
        }catch(err){
          console.log(err);
        }
      }
    },
    actions: {
      getTimeSeries({ commit }){
        if(timer == null){
          commit('getTimeSeries');
        }
        clearTimeout(timer);
        timer = setTimeout(() => {
          commit('getTimeSeries');
        }, 10 * 1000);
      },
      getAggregate({ commit }){
        commit('getAggregate');
      }
    },
    getters: {
      timeseries: state => state.timeseries,
      aggregate: state => state.aggregate
    }
  };
  