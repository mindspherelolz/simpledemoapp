/* eslint-disable */
import axios from 'axios';

// variables
let assestId = 'ed68e42a946441bc8b2a2a54ebb174b2';
let aspectType = 'aspect_Virtual_Motor_Condition';

//functions
export default {
    methods: {
        async getAggregateData() {
            let date = new Date().toISOString().slice(0,10);
            try{
                let res = await axios.get(`/api/iottsaggregates/v3/aggregates/${assestId}/${aspectType}?from=${date}T00:00:00%2B01:00&to=${date}T23:00:00%2B01:00&intervalValue=1&intervalUnit=hour`);
                let data = res.data;
                return data; 
            }catch(err) {
                console.log(err);
            }
        },
        async getTimeSeriesData() {
            try {
                let res = await axios.get(`/api/iottimeseries/v3/timeseries/${assestId}/${aspectType}`);
                let data = res.data
                return data;
            }catch(err) {
                console.log(err);
            }
        }
    }
}