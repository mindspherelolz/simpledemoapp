/* eslint-disable */
import httpRequest from 'request';

export default {
    methods: {
        async uploadFile(file){
            try{
                console.log(file);
                let headers =  {'Content-Type': 'application/octet-stream','type': 'image/*'}
                // let options = {
                //     method: 'PUT',
                //     headers: headers,
                //     body: file,
                // }
                // let body = Buffer.from(file);
                let res = await this.$axios.put('/api/iotfile/v3/files/ed68e42a946441bc8b2a2a54ebb174b2/Testing', file, headers);
                console.log(res);
                return res;
            }catch(e){
                console.log(e);
            }
        },
        uploadFileTest(file){
            httpRequest.put({
                url: '/api/iotfile/v3/files/ed68e42a946441bc8b2a2a54ebb174b2/Testing',
                headers: { 'content-type': 'application/octet-stream', 'type': 'image/*' },
                json: file
            }, function (error, answer, body) {
                if (!error && answer.statusCode == 204 || 200 || 201) {
                    console.log(body);
                } else {
                    console.log(error);
                }
            });
        }
    }
}