/* eslint-disable */
var assestId = 'ed68e42a946441bc8b2a2a54ebb174b2';

export default {
    data(){
        return {
            events: []
        }
    },
    methods: {
        async getEvents(){
            try{
                let res = await this.$axios.get('/api/eventmanagement/v3/events/');
                if(this.events != res.data && res.data._embedded.events != undefined){
                    this.events = res.data._embedded.events;
                    this.events = this.events.filter((event) => {
                        return event.entityId == assestId;
                    })
                }
                return res.data;
            }catch(e){
                console.log(e);
            }
        },
        async createEvent(event){
            try{
                this.date = new Date(this.date);
                this.date = new Date(this.date.getTime() - this.date.getTimezoneOffset() * 60000).toISOString();
                event.timestamp = this.date;
                this.selected = parseInt(this.selected);
                event.severity = this.selected;
                let res = await this.$axios.post('/api/eventmanagement/v3/events/', event);
                if(res.data){
                    alert("Create Successfully!!");
                }
                return res.data;
            }catch(e){
                console.log(e);
            }
        }
    }
}