// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'expose-loader?jQuery!jquery' // eslint-disable-line
import 'expose-loader?$!jquery' // eslint-disable-line
import 'vue-material/dist/vue-material.min.css'
import 'leaflet/dist/leaflet.css';
import Vue from 'vue';
import VCalendar from 'v-calendar';
import BootstrapVue from 'bootstrap-vue';
import VueMaterial from 'vue-material';
import VueApexCharts from 'vue-apexcharts'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';

import store from './store';
import router from './Routes';
import App from './App';
import axios from 'axios';
import VueAxios from 'vue-axios';

import api from './services/api'

Vue.component('apexchart', VueApexCharts);
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);
Vue.use(VueMaterial);
Vue.use(VCalendar, {
  firstDayOfWeek: 2,  // Monday
});
Vue.mixin(api);
/* eslint-disable */
Vue.config.productionTip = false;
// axios.defaults.baseURL = 'http://localhost:3005';
Vue.prototype.$axios = axios;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});
